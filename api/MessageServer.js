import request from '@/api/request.js';
/**
 * ===========
 * 消息服务接口
 * ===========
 */


/**
 * 获取未读消息数
 */
export async function getMessageNoReadCount() {
	return await request('/Message/GetMessageNoRead')
}

/**
 * 获取用户公告消息列表
 * @param {Object} params 参数 {page,count }
 */
export async function getMessageNoticeList(params = {
	page: 1,
	count: 20,
}) {
	return await request('/Message/GetNoticeListAndClear', 'get', params)
}

/**
 * 获取用户点赞、评论、提醒消息列表
 * @param {Object} params 参数 {page,count,type }
 * 类型 top 点赞、comment 评论、remind 提醒、
 */
export async function getMessageListByType(params = {
	page: 1,
	count: 20,
	type: 'comment'
}) {
	return await request('/Message/GetMessageList', 'get', params)
}
