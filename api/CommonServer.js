import request from '@/api/request.js';
/**
 * ===========
 * 常见公共服务接口
 * ===========
 */

/**
 * 加密需要解析的字符串 -视频、密码
 * @param {String} text 需要加密的字符串
 */
export async function getRsaText(text = '你好') {
  return await request(`/Rsa?text=${text}`)
}

/**
 * 主页banner与热门话题
 */
export async function getBannerTopicList() {
  return await request('/System/GetDefault')
}

/**
 * APP启动封面图
 */
export async function getStarCover() {
  return await request('/System/GetPosterInfoByAppStart?count=3')
}

/**
 * APP启动检查更新 
 * @param {Object} params 参数 {num:65,client:'android' }
 * num 版本号
 */
export async function getAppNewVersion(params = {
  num: 65,
  client: 'android'
}) {
  return await request('/System/GetAppVersionForNew', 'get', params)
}

/**
 * 获取省市列表
 * @param {Number} parentid 参数 0 省列表 、 通过省ID的市列表 
 */
export async function getCityList(parentid = 0) {
  return await request('/System/GetCityList', 'get', {
    parentid
  })
}

/**
 * 获取软件一些协议规则
 * @param {Number} key 参数  
 * hui_rule 社区公约、 hanbirule 汉币规则 orgjoin 组织入驻协议、shopjoin 商家入驻协议
 */
export async function getSysParamValue(key = 'hanbirule') {
  return await request('/System/GetSysParamValue', 'get', {
    key
  })
}

/**
 * 获取又拍云图片上传授权
 * @param {Object} params 参数 {filesize:62381,filetype:'jpg' }
 */
export async function getUpyunAuth(params = {
  filesize: 62381,
  filetype: 'jpg'
}) {
  return await request('/Upload/GetUpyunAuthention', 'get', params)
}
