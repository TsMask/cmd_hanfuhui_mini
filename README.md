# uni-app仿制汉服荟-基于APP 4.7.0原型

![书影o流风回雪-晚来天欲雪_700x.jpg](https://pic.hanfugou.com/android/2019/11/3/83b49515c8de4deeb312183cb881292c.jpg_700x.jpg) 

> `uni-app` 是一个使用 Vue.js 开发所有前端应用的框架，开发者编写一套代码，可发布到iOS、Android、H5、以及各种小程序（微信/支付宝/百度/头条/QQ/钉钉）等多个平台。

## 项目预览图

![汉服荟-APP启动封面](https://img-cdn-aliyun.dcloud.net.cn/stream/plugin_screens/2b37d180-2640-11ea-99e8-07e34d2572d4_0.png) 
![汉服荟-APP](https://img-cdn-aliyun.dcloud.net.cn/stream/plugin_screens/2b37d180-2640-11ea-99e8-07e34d2572d4_1.png) 
![汉服荟-微信小程序](https://img-cdn-aliyun.dcloud.net.cn/stream/plugin_screens/2b37d180-2640-11ea-99e8-07e34d2572d4_2.png)
![汉服荟-H5](https://img-cdn-aliyun.dcloud.net.cn/stream/plugin_screens/2b37d180-2640-11ea-99e8-07e34d2572d4_3.png) 

## 接口说明

> *保持学习的态度，不污染他人数据。*

为方便应用预览使用在线模拟应用数据，原先的版本使用Easy-Mock，更新版使用RAP2。

该项目没有开发后端服务，有兴趣可以自行开发后端接口对接。

## 原型完成程度：
* 动态、关注、发布列表显示
* 荟吧、摄影、视频、话题、文章、活动列表显示
* 评论、点赞、回复功能
* 个人信息、背景封面、个人头像、密码修改
* 关注、黑名单、排行榜、处罚公示、汉币兑换、签到 
* 视频播放、图片预览

> 该项目停止开发

## 项目说明

```text
cmd_hanfuhui_mini
┌─api                   RestfulAPI接口服务目录 
├─components            复用组件目录 
├─pages                 业务页面文件存放的目录
├─static                存放应用引用静态资源的目录 
├─store                 全局vuex数据仓库文件目录 
├─style                 全局公共样式文件目录 
├─unpackage             存放编译生成的文件目录 
├─utils                 常用函数工具文件目录 
├─config.js             配置网络地址
├─main.js               Vue初始化入口文件 
├─App.vue               应用配置，用来配置App全局样式以及监听 应用生命周期 
├─manifest.json         配置应用名称、appid、logo、版本等打包信息 
└─pages.json            配置页面路由、导航条、选项卡等页面类信息 
```

**又拍云图片后缀跟压缩显示** 
* 头像100：_100x100.jpg
* 头像200：_200x200.jpg
* 压缩预览图：_min.jpg/format/webp
* 高斯模糊图：_500x200.blur.jpg/format/webp
* 列表缩放图：_200x200.jpg/format/webp
* 列表缩放图：_250x250.jpg/format/webp
* 查看大图：_700x.jpg/format/webp
* 查看原图：_0.jpg/format/webp
* 保存原图：_0.jpg
* 视频封面图：_850x300.jpg/format/webp 

![好仙一颗丹药-【惊鸿】](https://pic.hanfugou.com/android/2019/9/31/565f2a1dbefd4f1788e16acea27f746a.jpg_700x.jpg) 

## 总结

主要微信小程序，条件编译打包APP并调整H5。开发APP端的建议使用 `nvue` 页面结构同 vue, 由 template、style、script 构成。即将推出的v3大幅度增强原生渲染APP。查看 [uni-app 跨端开发注意事项](https://uniapp.dcloud.io/matter) ，部分跨端使用[条件编译](https://uniapp.dcloud.io/platform)调整。

已发布到[uni-app 插件市场](https://ext.dcloud.net.cn/plugin?id=1150)，欢迎给个五星好评。    
[mescroll -支持uni-app的下拉刷新上拉加载组件](https://ext.dcloud.net.cn/plugin?id=343) v1.1.9（2019-12-16）  
[uParse修复版-html富文本加载](https://ext.dcloud.net.cn/plugin?id=364) v1.1.0（2019-06-30）  
[SwipeAction 滑动操作](https://ext.dcloud.net.cn/plugin?id=181) v1.1.0（2019-12-04）  
感谢以上组件的作者，以上插件可能都已经更新，做出大量优化。  

## 衍生项目-学习圈

1. 数据模拟使用[RAP2](http://rap2api.taobao.org/app/mock/283591/index)，图片没法上传，请求数据固定默认成功。
2. 二次优化开发毕业设计软件应用类，已完成校内知网论文查重*0.3%*和验收工作。
3. 后续不在开发更新，需要源码和论文可[联系](http://rap2api.taobao.org/app/mock/283591/index)*（拒绝白嫖）*。

**使用技术**
- 服务端：[Egg.js](https://eggjs.org/zh-cn/)、[MySQL 8.0.16](https://www.mysql.com/downloads/) （支持Dockerfile部署服务）
- 后台管理：[vue-element-admin](https://panjiachen.gitee.io/vue-element-admin-site/zh/) 精简版
- 客户端：[uni-app](https://uniapp.dcloud.io/) 已测试适配微信小程序/安卓APP/H5
- 第三方SDK：[阿里云对象存储](https://help.aliyun.com/document_detail/31883.html?spm=a2c4g.11186623.6.595.6fa73ff7DgzTjb)、[阿里云文本审核](https://help.aliyun.com/document_detail/70439.htm?spm=a2c4g.11186623.2.85.1246c4374XP7bv#reference-vsg-xmh-w2b)、[网易云信IM即时通讯](https://yunxin.163.com/)

**预览**
- [用户端H5](http://static-9a23d253-58d3-40bf-92fa-d7a13b690a33.bspapp.com/learn_circle/#/)
- [用户端APP](https://static-9a23d253-58d3-40bf-92fa-d7a13b690a33.bspapp.com/app/learn_circle_1.0.0.apk)
- [后台管理](http://static-9a23d253-58d3-40bf-92fa-d7a13b690a33.bspapp.com/learn_admin/#/)
   
> 示例应用 `HBuilderX 3.1.13.20210514` 版编译。    

**祝愿DCloud越做越好**
